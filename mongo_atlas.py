import json
import pymongo

archivo = "/home/lowenhard/colegios_colombia.json"
cliente = pymongo.MongoClient("mongodb://mongoso:soRok.40.forty@cluster0-shard-00-00-ej7ez.mongodb.net:27017,cluster0-shard-00-01-ej7ez.mongodb.net:27017,cluster0-shard-00-02-ej7ez.mongodb.net:27017/<DATABASE>?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin")
db = cliente.test

def file_toDict():
    """
    Convierte el archivo de datos de colegios a diccionario
    """
    with open(archivo) as jsonfile:
        datos = json.load(jsonfile)
    return datos

def test_atlas():
    coleccion = db.schule
    cursor = coleccion.find()
    for elemento in cursor:
        print(elemento)

def insert_atlas():
    datos = file_toDict()
    col = db.colegios
    conteo = 0
    for dato in datos:
        col.insert_one(dato)
        conteo += 1
    print("Se insertaron {0} documentos".format(conteo))
